import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@material-ui/core";
import ButtonForm from "../../form_elements/Button";
import useAuth from "../../Auth/Auth";
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const TableData = (props) => {
  const classes = useStyles();
  const { logout } = useAuth();
  return (
    <Grid container item spacing={2}>
      <Grid container item justify="space-between">
        <ButtonForm
          name="Agregar Ticket"
          onClick={() => {
            props.handle.agregar(true);
          }}
        />
        <ButtonForm name="Logout" width="15%" onClick={logout} />
      </Grid>
      <Grid container item>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell>Id Ticket</TableCell>
                <TableCell>User</TableCell>
                <TableCell align="center">Ticket pedido</TableCell>

                <TableCell align="center" padding="none"></TableCell>
                <TableCell align="center" padding="none"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.data.map((value, index) => (
                <TableRow key={index}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>{value.id}</TableCell>
                  <TableCell>
                    {value.usuario !== null ? value.usuario.mail : ""}
                  </TableCell>
                  <TableCell align="center">
                    {value.ticket_pedido ? "si" : "no"}
                  </TableCell>

                  <TableCell align="right" size="small" width="20px">
                    {
                      <ButtonForm
                        name="Editar"
                        width="50%"
                        onClick={() => {
                          props.handle.agregar(true, value);
                        }}
                      />
                    }
                  </TableCell>
                  <TableCell align="right" size="small" width="20px">
                    {
                      <ButtonForm
                        name="Eliminar"
                        width="50%"
                        onClick={() => {
                          props.handle.delete(value);
                        }}
                      />
                    }
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};

export default TableData;
