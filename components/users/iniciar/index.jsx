import React from "react";
import Input from "../../form_elements/Input";
import { Grid } from "@material-ui/core";
import ButtonForm from "../../form_elements/Button";
import { formStyle } from "./styles";

const Registrarse = (props) => {
  const classes = formStyle();
  return (
    <Grid
      container
      item
      justify="center"
      component="form"
      onSubmit={props.handleSubmit}
      spacing={1}
      className={classes.main}
    >
      <Grid container item justify="center" style={{ minHeight: "200px" }}>
        <Input name="mail" type="text" label="Email" />
        <Input name="pass" type="password" label="Contrasena" width="80%" />
      </Grid>

      <ButtonForm
        name="Iniciar"
        type="submit"
        onClick={() => console.log("iniciando")}
      />
      <ButtonForm name="Limpiar" onClick={props.resetForm} />
    </Grid>
  );
};

export default Registrarse;
