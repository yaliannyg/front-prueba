import { makeStyles } from "@material-ui/core/styles";

export const fieldStyle = makeStyles(() => ({
  main: {
    margin: "0 auto ",
    width: (props) => (props.width ? props.width : "80%"),
    height: "25%",
  },
  labelStyle: {
    color: "#d33d3",
    letterSpacing: "2px",
    fontWeight: "bold",
    "&.Mui-focused": {
      color: "#23A5",
    },
  },
  inputStyle: {
    backgroundColor: "#ffa",
    borderRadius: "5px",
    height: "100%",
    padding: "0 15px",
    "&.Mui-focused": {
      backgroundColor: "#23A5",
    },
  },
  typographyStyle: {
    fontSize: "30px",
    letterSpacing: "3px",
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  selectStyle: {
    width: (props) => (props.width ? props.width : "30%"),
    marginRight: (props) => (props.marginRight ? props.marginRight : "")
  },
}));
