import React from "react";
import Router from "next/router";
import Cookies from "js-cookie";

const PrivateRoute = (Component) => {
  return () => {
    const user = Cookies.get("user");
    const tipo = Cookies.get("tipo");

    React.useEffect(() => {
      if (!user) {
        Router.push("/");
      } else Router.push("/dashboard");
    }, []);

    return <Component user={user} tipo={tipo} />;
  };
};

export default PrivateRoute;
