# Título del Proyecto

_Prueba_

### Pre-requisitos 📋
```
Instalar dependencias en los distintos repositorios
```
```
Iniciar servidor mysql
```
```
Crear base de datos de nombre "prueba" e importar archivo sql. Se encuentra en back-prueba (prueba.sql)
```
```
Iniciar server. Repositorio "back-prueba", comando npm run start
```

```
Iniciar server. Repositorio "front-prueba", comando npm run dev. (Se inicia http://localhost:3000/)
```
