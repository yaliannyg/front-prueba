import React from 'react'
import { TextField } from '@material-ui/core';
import {fieldStyle} from './styles'
import { useField } from "formik";


const Input = ({name, label, type, width}) => {
    
    const [field, meta] = useField({name:name, type:type});
    
    const classes = fieldStyle({width: width});
    return (
        <TextField
            id={name} 
            className = {classes.main}
            label= {label}
            error = {meta.error && meta.touched}
            helperText={meta.touched? meta.error: undefined}
            variant = 'standard'
            type = {type}
            value = {meta.value}
            onChange = {field.onChange}
            InputProps={{ disableUnderline: true, classes: {
              root: classes.inputStyle
            } }}
            InputLabelProps={{
              shrink: true,
                classes: {
                  root: classes.labelStyle,
                }
              }}
        />
    )

}

export default Input;