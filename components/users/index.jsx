import React from "react";
import { Grid } from "@material-ui/core";
import { formStyle } from "./styles";
import { default as Iniciar } from "../users/iniciar/formFormik";
import { default as Registrar } from "../users/registrarse/formFormik";
import ButtonForm from "../form_elements/Button";

const Users = (props) => {
  const classes = formStyle();
  const [flag, setFlag] = React.useState(false);
  const [label, setLabel] = React.useState("");
  const onClic = (name) => {
    setFlag(name === "Registrar Usuario" ? true : false);
  };

  return (
    <Grid container>
      <Grid item sm={2} />
      <Grid
        item
        container
        sm={8}
        className={classes.container}
        component="div"
        onSubmit={props.handleSubmit}
        spacing={2}
        justify="center"
      >
        <Grid container item justify="center">
          <ButtonForm name="Iniciar Usuario" color="#245" onClick={onClic} />
          <ButtonForm name="Registrar Usuario" color="#245" onClick={onClic} />
        </Grid>

        {flag === true ? <Registrar handleLabel={setLabel}/> : <Iniciar handleLabel={setLabel}/>}
        <Grid container item justify="center">
          <label style={{marginTop:"5px", color:"red"}}>{label}</label>
        </Grid>
      </Grid>
      <Grid item sm={2} />
    </Grid>
  );
};

export default Users;
