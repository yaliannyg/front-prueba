import axios from "axios";

export const createUser = async (data) => {
  const response = await axios({
    method: "post",
    url: "http://localhost:4000/api/users",
    data: data,
  });

  return response;
};

export const getUser = async (data) => {
    const response = await axios({
      method: "post",
      url: "http://localhost:4000/api/users/user",
      data: data,
    });
  
    return response;
  };

  export const getAllUsers = async () => {
    const response = await axios({
      method: "get",
      url: "http://localhost:4000/api/users",

    });
  
    return response;
  };

  export const findById = async (data) => {
    console.log(data)
    const response = await axios({
      method: "get",
      url: `http://localhost:4000/api/tipo/${data.id}`,
    });
  
    return response;
  };

export const getAllTickets = async () => {
    const response = await axios({
      method: "get",
      url: "http://localhost:4000/api/tickets",

    });
  
    return response;
  };

  export const getTicketsFree = async (data) => {
    const response = await axios({
      method: "get",
      url: `http://localhost:4000/api/tickets/free`,
    });
  
    return response;
  };


  export const getTickets = async (data) => {
    const response = await axios({
      method: "get",
      url: `http://localhost:4000/api/tickets/${data.id_user}`,
    });
  
    return response;
  };
  
  export const createTickets = async (data) => {
    const response = await axios({
      method: "post",
      url: "http://localhost:4000/api/tickets",
      data: data,
    });
  
    return response;
  };

  export const updateTicket = async (data) => {
    console.log(data)
    const response = await axios({
      method: "put",
      url: `http://localhost:4000/api/tickets/${data.id}`,
      data: data,
    });
  
    return response;
  };

  export const deleteTicket = async (data) => {
    const response = await axios({
      method: "delete",
      url: `http://localhost:4000/api/tickets/${data.id}`,
    });
  
    return response;
  };
   