import React, { Component } from "react";
import { getTicketsFree, updateTicket, getTickets } from "../../../api";
import TableData from "./ticketsStyle";

import { Grid } from "@material-ui/core";

class Tickets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mine: false,
      tickets: [],
    };
  }

  handleMisTickets(value, data) {
    if (value)
      getTickets({ id_user: this.props.user })
        .then((data) => {
          this.setState({
            tickets: data.data,
            mine: value,
          });
        })
        .catch((error) => {
          console.log(error);
          return;
        });
    else
      getTicketsFree({ id_user: this.props.user })
        .then((data) => {
          this.setState({
            tickets: data.data,
            mine: value,
          });
        })
        .catch((error) => {
          console.log(error);
          return;
        });
  }

  actPedido(id) {
    updateTicket({ id: id, id_user: this.props.user, ticket_pedido: true })
      .then((response) => {
        getTicketsFree({ id_user: this.props.user })
          .then((data) => {
            this.setState({
              tickets: data.data,
              
            });
          })
          .catch((error) => {
            console.log(error);
            return;
          });
      })
      .catch((error) => {
        console.log(error);
        return;
      });
  }

  componentDidMount() {
    getTicketsFree({ id_user: this.props.user })
      .then((data) => {
        this.setState({
          tickets: data.data,
        });
      })
      .catch((error) => {
        console.log(error);
        return;
      });
  }

  render() {
    return (
      <Grid container style={{ padding: "20px" }} spacing={2}>
        <TableData
          data={{ tickets: this.state.tickets, mine: this.state.mine }}
          handle={{
            cambiar: this.actPedido.bind(this),
            misTickets: this.handleMisTickets.bind(this),
          }}
        />
      </Grid>
    );
  }
}

export default Tickets;
