import React from "react";
import { default as Usuario } from "../components/tickets/user";
import { default as Administrador } from "../components/tickets/admin";
import PrivateRoute from "../components/Auth/privateRoute";

const Dashboard = (props) => {
  const [tipo, setTipo] = React.useState();
  React.useEffect(() => {
    if (props.tipo)
      if (props.tipo === "Admin") setTipo("Admin");
      else setTipo("User");
  }, []);

  return (
    <>{tipo === "Admin" ? <Administrador /> : <Usuario user={props.user} />}</>
  );
};

export default PrivateRoute(Dashboard);
