export const validation = (values) => {
  let errors = {};

  if (!values.pass) {
    errors.pass = "Apellido es requerido!";
  }
  if (!values.mail) {
    errors.mail = "Email es requerido!";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.mail)) {
    errors.mail = "Email invalido";
  }
  return errors;
};
