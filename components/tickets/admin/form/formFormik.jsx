import React from "react";
import { Formik } from "formik";
import { createTickets, getAllUsers } from "../../../../api";
import Ticket from "./index";

const initialValues = (initialData) => {
  return {
    mail: initialData? initialData.usuario ? initialData.usuario.mail : "" : "",
  };
};

const FormFormik = ({ handle, initialData }) => {
  const [data, setData] = React.useState([]);
  React.useEffect(() => {
    getAllUsers()
      .then((response) => {
        let mails = [];
        response.data.map((value) => {
          mails.push(value.mail);
        });
        setData(mails);
      })
      .catch((error) => {
        console.log(error);
        return;
      });
  }, []);

  return (
    <Formik
      initialValues={initialValues(initialData)}
      onSubmit={(values, actions) => {
        initialData
          ? handle.update({
              id: initialData.id,
              mail: values.mail,
            })
          : createTickets({
              mail: values.mail,
            })
              .then((data) => {
                handle.crear(data.data);
                handle.agregar(false);
              })
              .catch((error) => {
                console.log(error);
                return;
              });

        actions.resetForm();
        actions.setSubmitting(false);
      }}
    >
      {(props) => <Ticket {...props} handle={handle} data={data} />}
    </Formik>
  );
};

export default FormFormik;
