import {  Button } from "@material-ui/core";
import {buttonStyle} from './styles'
const ButtonForm = ({name, type, onClick, width, color}) => {
  
    const classes = buttonStyle({width:width, color:color});
  return (
    <Button
      color="secondary"
      variant="contained"
      
      type={type}
      className = {classes.main}
      onClick={()=>{onClick(name)}}
    >
      {name}
    </Button>
  );
};

export default ButtonForm;