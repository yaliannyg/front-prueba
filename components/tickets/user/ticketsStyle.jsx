import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@material-ui/core";
import ButtonForm from "../../form_elements/Button";
import useAuth from "../../Auth/Auth";
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const TableData = (props) => {
  const classes = useStyles();
  const { logout } = useAuth();
  return (
    <Grid container item spacing={2}>
      <Grid container item justify="space-between">
      <ButtonForm
          name={props.data.mine?"Todos los Tickets":"Mis Tickets"}
          width="25%"
          onClick={() => {
            props.handle.misTickets(!props.data.mine);
          }}
        />
        <ButtonForm name="Logout" width="15%" onClick={logout} />
      </Grid>
      <Grid container item>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell>Id Ticket</TableCell>
                <TableCell
                  align="right"
                  padding="none"
                  size="small"
                ></TableCell>
                <TableCell
                  align="right"
                  padding="none"
                  size="small"
                ></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.data.tickets.map((value, index) => (
                <TableRow key={value.id}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>{value.id}</TableCell>

                  <TableCell align="right" size="small">
                    {
                      props.data.mine?
                     "": <ButtonForm
                     name="Pedir Ticket"
                     width="35%"
                     onClick={() => {
                       props.handle.cambiar(value.id);
                     }}
                   />
                    }
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};

export default TableData;
