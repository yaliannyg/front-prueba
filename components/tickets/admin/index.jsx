import React, { Component } from "react";
import { getAllTickets, updateTicket, deleteTicket } from "../../../api";
import TableData from "./ticketStyle";
import FormFormik from "./form/formFormik";
import { Grid } from "@material-ui/core";
class Tickets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      agregar: false,
      initialValues: undefined,
      tickets: [],
    };
  }

  handleAgregar(value, data) {
    this.setState({
      agregar: value,
      initialValues: data,
    });
  }

  addTicket(data) {
    this.setState({
      tickets: [data,...this.state.tickets]
    });
  }

  actTicket(data) {
    updateTicket(data)
      .then((response) => {
        getAllTickets()
          .then((response) => {
            console.log(response);
            this.setState({
              tickets: response.data,
              agregar: false,
            });
          })
          .catch((error) => {
            console.log(error);
            return;
          });
      })
      .catch((error) => {
        console.log(error);
        return;
      });
  }

  delTicket(data) {
    deleteTicket(data)
      .then((response) => {
        getAllTickets()
          .then((response) => {
            // console.log(data)
            this.setState({
              tickets: response.data,
              agregar: false,
            });
          })
          .catch((error) => {
            console.log(error);
            return;
          });
      })
      .catch((error) => {
        console.log(error);
        return;
      });
  }
  componentDidMount() {
    getAllTickets()
      .then((data) => {
        // console.log(data)
        this.setState({
          tickets: data.data,
        });
      })
      .catch((error) => {
        console.log(error);
        return;
      });
  }

  render() {
    return (
      <Grid container style={{ padding: "20px" }} spacing={2}>
        {this.state.agregar === true ? (
          <FormFormik
            initialData={this.state.initialValues}
            handle={{
              agregar: this.handleAgregar.bind(this),
              crear: this.addTicket.bind(this),
              update: this.actTicket.bind(this),
            }}
          />
        ) : (
          <TableData
            data={this.state.tickets}
            handle={{
              agregar: this.handleAgregar.bind(this),
              delete: this.delTicket.bind(this),
            }}
          />
        )}
      </Grid>
    );
  }
}

export default Tickets;
