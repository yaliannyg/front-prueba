import React from "react";
import { Formik } from "formik";
import { validation } from "./validate";
import Registrarse from "../iniciar/index";
import { getUser } from "../../../api";
import useAuth from "../../Auth/Auth";

const initialValues = () => {
  return {
    mail: "",
    pass: "",
  };
};

const FormFormik = (props) => {
  const { login } = useAuth();
  return (
    <Formik
      initialValues={initialValues()}
      onSubmit={(values, actions) => {
        getUser({
          mail: values.mail,
          pass: values.pass,
        })
          .then((data) => {
            login(data);
          })
          .catch((error) => {
            if(error.response)
            props.handleLabel(error.response.data.message)
            console.log(error)
            return;
          });

        actions.resetForm();
        actions.setSubmitting(false);
      }}
      validate={validation}
    >
      {(props) => <Registrarse {...props} />}
    </Formik>
  );
};

export default FormFormik;
