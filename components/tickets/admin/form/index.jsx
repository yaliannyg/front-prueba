import React from "react";
import Input from "../../../form_elements/Input";
import SelectInput from "../../../form_elements/Input/selectedInput";
import { Grid } from "@material-ui/core";
import ButtonForm from "../../../form_elements/Button";
import { formStyle } from "./styles";

const Ticket = (props) => {
  const classes = formStyle();
  return (
    <Grid container item>
      <Grid item sm={4} />
      <Grid
        container
        item
        justify="center"
        component="form"
        onSubmit={props.handleSubmit}
        spacing={1}
        className={classes.main}
        sm={4}
      >
        <Grid
          container
          item
          justify="space-around"
          direction="column"
          alignContent="center"
          style={{ minHeight: "200px", padding: "15px 0 " }}
        >
          <SelectInput
            name="mail"
            label="Usuario"
            values={props.data}
            width="70%"
          />
        </Grid>

        <ButtonForm
          name="Enviar"
          type="submit"
          onClick={() => console.log("creando ticket")}
        />
        <ButtonForm name="Volver" onClick={() => props.handle.agregar(false)} />
      </Grid>
      <Grid item sm={4} />
    </Grid>
  );
};

export default Ticket;
