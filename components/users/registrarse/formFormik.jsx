import React from "react";
import { Formik } from "formik";
import { validation } from "./validate";
import Registrarse from "./index";
import { createUser } from "../../../api";
import useAuth from "../../Auth/Auth";

const initialValues = () => {
  return {
    nombre: "",
    mail: "",
    pass: "",
    id_tipouser: "Admin",
  };
};

const FormFormik = (props) => {
  const { login } = useAuth();
  return (
    <Formik
      initialValues={initialValues()}
      onSubmit={(values, actions) => {
        createUser({
          nombre: values.nombre,
          mail: values.mail,
          pass: values.pass,
          id_tipouser: values.id_tipouser,
        })
          .then((data) => {
            console.log(data)
            login(data);
          })
          .catch((error) => {
            if(error.response)
              props.handleLabel(error.response.data.message)
              console.log(error)
            
            return;
          });

        actions.resetForm();
        actions.setSubmitting(false);
      }}
      validate={validation}
    >
      {(props) => <Registrarse {...props} />}
    </Formik>
  );
};

export default FormFormik;
