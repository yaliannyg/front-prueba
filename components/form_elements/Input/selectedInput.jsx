import React from "react";
import { MenuItem, InputLabel, FormControl, Select } from "@material-ui/core";
import { useField } from "formik";
import { fieldStyle } from "./styles";
const SelectInput = ({ label, name, values, width,marginRight }) => {
  const [field, meta] = useField({ name: name });

  const classes = fieldStyle({ width: width, marginRight:marginRight});

  return (
    <FormControl id={name} className={classes.selectStyle}>
      <InputLabel>{label}</InputLabel>
      <Select
        labelId="selectInputLabel"
        id={name}
        value={meta.value}
        onChange={field.onChange(name)}
      >
        {values.map((value) => {
          return (
            <MenuItem key={value} value={value}>
              {value}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};

export default SelectInput;
