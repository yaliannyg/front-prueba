import React, { createContext, useState, useContext } from "react";
import Router from "next/router";
import Cookies from "js-cookie";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const getUser = () => {
    const user = Cookies.get("user");
    if (user) Router.push("/dashboard");
  };
  const login = (data) => {
    
    Cookies.set("user", data.data.id);
    Cookies.set("tipo", data.data.tipo_usuario.nombre);
    Router.push("/dashboard");
  };

  const logout = ()=>{
    Cookies.remove("user");
    Cookies.remove("tipo");
    Router.push("/");

  }

  return (
    <AuthContext.Provider value={{ login, getUser, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export default function useAuth() {
  const context = useContext(AuthContext);
  return context;
}
