import React from "react";

import Users from "../components/users/index";

import PrivateRoute from "../components/Auth/privateRoute";
const Home = () => {
  return <Users />;
};

export default PrivateRoute(Home);
