import React from "react";
import Input from "../../form_elements/Input";
import { Grid } from "@material-ui/core";
import ButtonForm from "../../form_elements/Button";
import { formStyle } from "./styles";
import SelectInput from "../../form_elements/Input/selectedInput";
const Registrarse = (props) => {
  const classes = formStyle();

  return (
    <Grid
      container
      item
      justify="center"
      component="form"
      onSubmit={props.handleSubmit}
      spacing={1}
      className={classes.main}
    >
      <Grid container item justify="center" style={{ minHeight: "200px" }}>
        <Input name="nombre" type="text" label="Nombre" />
        <Input name="mail" type="text" label="Email" />
        <Input name="pass" type="password" label="Contraseña" width="40%" />
        <SelectInput
          name="id_tipouser"
          label="Tipo de Usuario"
          values={["Admin", "User"]}
          width="30%"
          marginRight="10%"
        />
      </Grid>

      <ButtonForm
        name="Registrarse"
        type="submit"
        onClick={() => {
          console.log("registrando");
        }}
      />
      <ButtonForm name="Limpiar" onClick={props.resetForm} />
    </Grid>
  );
};

export default Registrarse;
