export const validation = (values) => {
    let errors = {};

    if (!values.nombre || values.nombre.length <= 1) {
        errors.nombre = 'Nombre es requerido!';
    } 
    if (!values.pass || values.pass.length <= 5) {
        errors.pass = 'min 6 caracteres';
    }
    if (!values.mail) {
        errors.mail = 'Email es requerido!';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.mail)) {
        errors.mail = 'Email invalido';
    }
    return errors;
}