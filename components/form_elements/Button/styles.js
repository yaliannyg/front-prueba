import { makeStyles } from "@material-ui/core/styles";

export const buttonStyle = makeStyles(() => ({
  main: {
    margin: "0 25px ",
    width: (props) => (props.width ? props.width: "30%"),
    backgroundColor:  (props) => (props.color ? props.color: "#23A5"),
  },
}));
