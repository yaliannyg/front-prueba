import { makeStyles } from '@material-ui/core/styles';

export const formStyle = makeStyles(()=>({
    main: {
        marginTop: '50px',
        padding: '15px 0',
        boxShadow:'1px 3px 5px 2px rgba(255, 105, 135, .3)'
    },
    container:{
        // border:"1px solid black",
        marginTop:"10px"

    },
    buttonStyle: {
        backgroundColor: 'rgba(255, 105, 135, .3)',
        width: '33%',
        fontWeight: "bold",
        textTransform: "uppercase",
        boxShadow:'0px 1px 1px 2px rgba(0, 0, 0, .54)'
    },
    labelStyle: {
        color: 'rgba(0,0,0,0.54)',
        letterSpacing: '2px',
        fontWeight: "bold",
    },

  
}));
